﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingStar : MonoBehaviour
{
	public ParticleSystem[] ShootingStarSpots;
	private float timer;
	public float IntervalsMinimum;
	public float IntervalsMaximum;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		timer -= Time.deltaTime;

		//chooses a random shooting star position and shoots it off after the timer reaches 0
		if (timer <= 0)
		{
			ShootingStarSpots[Random.Range(0, ShootingStarSpots.Length)].Play();
			timer = Random.Range(IntervalsMinimum, IntervalsMaximum);
		}
	}
}
