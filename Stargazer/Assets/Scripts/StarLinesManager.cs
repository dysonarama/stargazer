﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Liminal.SDK.VR;
using Liminal.SDK.VR.Input;

public class StarLinesManager : MonoBehaviour
{

	public GameObject StarLine;
	public GameObject CurrentStar;
	private GameObject LastStar;
	public Vector3 MousePos;
	private Vector3 SlowPos;
	public LineRenderer CurrentLine;
	public GameObject LineCurrentPosParticle;
	private bool AlreadyOn;
	public AudioSource StarSound;
	private bool HasClicked;
	public GameObject EndingSpawnPos;
	private bool Ended;
	private int CurrentEndLine;

	public List<GameObject> Stars = new List<GameObject>();

	public List<Vector3> LinePosition = new List<Vector3>();

	public List<LineRenderer> lineRends = new List<LineRenderer>();

	// Use this for initialization
	void Start()
	{
		CurrentLine = null;
		LineCurrentPosParticle.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
		var primaryinput = VRDevice.Device.PrimaryInputDevice;

		SlowPos = Vector3.Lerp(SlowPos, MousePos, 0.08f);

		//If the Vr controllers main button is pressed
		if (OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown(KeyCode.Space))
		{
			//if there is a current line, and the player clicks somewhere that doesn't have a star, it'll delete the last line renderer position
			if (CurrentLine)
			{
				CurrentLine.positionCount -= 1;

				//Starts a fade when the consumer has finished their drawing.
				StartCoroutine(Fade(CurrentLine, 0, 5));

				//Add line before delete for showcasing at the end of the experience.
				lineRends.Add(CurrentLine);

				//remove the current line so that the player can create a new ibw
				CurrentLine = null;
				LinePosition.Clear();
				LineCurrentPosParticle.SetActive(false);

				//resets the bool to check whether the current particle is being used for the creation of an image so that future hovers turn back to their original size.
				HasClicked = false;

				//Resets the stars that make up the line renderer shape to their designated size. 
				for (int i = 0; i < Stars.Count; i++)
				{
					Stars[i].GetComponent<ParticleSystem>().startSize = 0.3f;
					if (i >= Stars.Count -1)
					{
						Stars.Clear();
					}
				}
			}

			if (CurrentStar)
			{
				//Instantiates a line renderer at the current reticle position and creates new positions for the line renderer to go.
				if (!CurrentLine)
				{
					//adds the last star to the stars list and then removes last stars variable so that the star stays the right size. 
					Stars.Add(LastStar);
					LastStar = null;

					//sets bool to check whether the particle is currently being made into a line so not to reduce its size on leaving the current star.
					HasClicked = true;


					StarSound.Play();
					CurrentLine = Instantiate(StarLine.GetComponent<LineRenderer>(), CurrentStar.transform.position, Quaternion.identity);
					LinePosition.Add(CurrentStar.transform.position);
					LinePosition.Add(CurrentStar.transform.position);
					CurrentLine.positionCount = 2;
					LineCurrentPosParticle.SetActive(true);
					SlowPos = MousePos;
				}
			}
		}

		// stars assigning positions for the line renderer if it is currently being manipulated. 
		if (LinePosition.Count > 0)
		{
			for (int i = 0; i < LinePosition.Count; i++)
			{

				//if there isn't a star beneath, the last line renderer position will follow the mouse
				if (!CurrentStar)
				{

					LinePosition[LinePosition.Count - 1] = Vector3.Lerp(LinePosition[LinePosition.Count - 1], SlowPos, 0.04f);
				}

				//if there is a star at the reticle, the last renderer position will snap to that position.
				else if (CurrentStar)
				{
					if (AlreadyOn)
					{
						LinePosition[LinePosition.Count - 1] = Vector3.Lerp(LinePosition[LinePosition.Count - 1], SlowPos, 0.04f);

					}
					else
					{
						StartCoroutine(MovingTo(CurrentLine, LinePosition.Count - 1, LinePosition[LinePosition.Count - 1], CurrentStar.transform.position, 0.5f));

					}
				}

				//sets the line renderer position to the previously created positions.
				CurrentLine.SetPosition(i, LinePosition[i]);
			}
		}

		// Adds an extra position for the line renderer.
		if (CurrentStar)
		{
			LastStar = CurrentStar;
			CurrentStar.GetComponent<ParticleSystem>().startSize = 1;
			if (CurrentLine)
			{
				if (!AlreadyOn)
				{
					//adds the last star to the stars list and then removes last stars variable so that the star stays the right size. 
					Stars.Add(LastStar);
					LastStar = null;


					StarSound.Play();
					LinePosition.Add(SlowPos);
					CurrentLine.positionCount += 1;
					AlreadyOn = true;
				}
			}
		}

		if (!CurrentStar)
		{
			//Checks if the last star was clicked on and if it is then to not change the particles size.
			if (!HasClicked)
			{
				if (LastStar)
				{
					LastStar.GetComponent<ParticleSystem>().startSize = 0.3f;
				}
			}
			AlreadyOn = false;
		}

		LineCurrentPosParticle.transform.position = MousePos;
	}

	// The last position of the line renderer that hit a star moves to the star it hit
	IEnumerator MovingTo(LineRenderer Lines, int MovingLineNumber, Vector3 StartingPos, Vector3 StarPos, float Duration)
	{
		float ElapsedTime = 0;

		while (ElapsedTime < Duration)
		{
			ElapsedTime += Time.deltaTime;
			LinePosition[MovingLineNumber] = Vector3.Lerp(StartingPos, StarPos, (ElapsedTime / Duration));
			//print(ElapsedTime);
			yield return new WaitForEndOfFrame();
		}
	}

	//A fade for line renderers that have been completed
	IEnumerator Fade(LineRenderer Lines, float EndingAlpha, float Duration)
	{
		if (Ended)
		{
			float OffsetX = Lines.GetPosition(0).x - EndingSpawnPos.transform.position.x;
			float OffsetZ = Lines.GetPosition(0).z - EndingSpawnPos.transform.position.z;

			for (int i = 0; i < Lines.positionCount; i++)
			{
				Lines.SetPosition(i, new Vector3(Lines.GetPosition(i).x - OffsetX, Lines.GetPosition(i).y, Lines.GetPosition(i).z - OffsetZ));
			}
		}

		Color BeginningColour = Lines.materials[0].color;

		float BeginningAlpha = BeginningColour.a;

		float elapsedTime = 0;

		while (elapsedTime < Duration)
		{
			Color NewColor = new Color(BeginningColour.r, BeginningColour.g, BeginningColour.b, Mathf.Lerp(BeginningAlpha, EndingAlpha, elapsedTime / Duration));
			Lines.material.color = NewColor;
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		StartCoroutine(Fade(lineRends[CurrentEndLine], 0, 3));

		//Sets up the wait timer so that the next line renderer doesnt fade in at the same time as the last one faded out. 
		yield return new WaitForSeconds(Duration);

		if (Ended)
		{
			if (CurrentEndLine <= lineRends.Count)
			{
				CurrentEndLine += 1;
				StartCoroutine(Fade(lineRends[CurrentEndLine], 1, 3));
			}
		}
	}

	public void Finish()
	{
		// creates a bool so the fade coroutine knows to count up the amount of lines the player has made.
		Ended = true;
		StartCoroutine(Fade(lineRends[0], 1, 3));
		print("Yep, done shit");
	}
}
