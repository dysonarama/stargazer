﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Liminal.SDK.VR.Avatars;
using Liminal.SDK.VR;

public class Interaction : MonoBehaviour
{
	public GameObject Star;
	public StarLinesManager Lines;
	public Vector3 RaycastDirection;
	private Radio RadioScript;
	public GameObject Reticle;


	// Use this for initialization
	void Start()
	{
		Reticle.SetActive(false);
		Lines = GameObject.Find("GameManager").GetComponent<StarLinesManager>();
		RadioScript = GameObject.Find("GameManager").GetComponent<Radio>();
	}

	// Update is called once per frame
	void Update()
	{
		//Finds the transform of the hand and its direction
		var pointer = VRAvatar.Active.PrimaryHand.GetControllerVisual().PointerVisual.Pointer.Transform;
		RaycastHit hit;

		//Creates a raycast forward and asks the question of whether it is hitting anything or not.
		if (Physics.Raycast(pointer.position, pointer.forward, out hit, Mathf.Infinity))
		{
			//Changes the StarLinesManager's Mouse position to equal the point that the raycast is hitting. 
			Lines.MousePos = hit.point;

			//Asks if it's hitting a star and then changes the line managers current star to it. 
			if (hit.collider.name == "Star(Clone)")
			{
				Reticle.SetActive(true);
				Lines.CurrentStar = hit.collider.gameObject;
				Debug.DrawRay(pointer.position, pointer.forward * hit.distance, Color.green);
				hit.collider.gameObject.GetComponent<ParticleSystem>().startSize = 1;
			}

			//Setting up radio events
			else if(hit.collider.name == "UpArrow")
			{
				Reticle.SetActive(true);
				RadioScript.OnUp = true;
				RadioScript.OnDown = false;
				RadioScript.OnRight = false;
				RadioScript.OnLeft = false;
			}

			else if(hit.collider.name == "DownArrow")
			{
				Reticle.SetActive(true);
				RadioScript.OnUp = false;
				RadioScript.OnDown = true;
				RadioScript.OnRight = false;
				RadioScript.OnLeft = false;
			}

			else if (hit.collider.name == "RightArrow")
			{
				Reticle.SetActive(true);
				RadioScript.OnUp = false;
				RadioScript.OnDown = false;
				RadioScript.OnRight = true;
				RadioScript.OnLeft = false;
			}

			else if(hit.collider.name =="LeftArrow")
			{
				Reticle.SetActive(true);
				RadioScript.OnUp = false;
				RadioScript.OnDown = false;
				RadioScript.OnRight = false;
				RadioScript.OnLeft = true;
			}

			// terrible code for finding the ready up button to begin the experience.
			else if (hit.collider.name == "ReadyUp")
			{
				Reticle.SetActive(true);
				var primaryinput = VRDevice.Device.PrimaryInputDevice;

				//If the Vr controllers main button is pressed
				if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.Space) || OVRInput.GetUp(OVRInput.Button.One))
				{
					GameObject.Find("GameManager").GetComponent<BeginningSetup>().StartExperience();
				}
			}

			// my terrible code for finding the continue button in scene.
			else if(hit.collider.name == "Continue")
			{
				var primaryinput = VRDevice.Device.PrimaryInputDevice;

				//If the Vr controllers main button is pressed
				if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.Space) || OVRInput.GetUp(OVRInput.Button.One))
				{
					GameObject.Find("GameManager").GetComponent<BeginningSetup>().Continue();
				}
			}

			// my terrible code for finding the finish button in the scene.
			else if(hit.collider.name == "Finish")
			{
				var primaryinput = VRDevice.Device.PrimaryInputDevice;

				//If the Vr controllers main button is pressed
				if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.Space) || OVRInput.GetUp(OVRInput.Button.One))
				{
					GameObject.Find("GameManager").GetComponent<StarLinesManager>().Finish();
				}
			}
			//If it isn't hitting a star it tells the line manager that there is no star there currently.
			else
			{
				Reticle.SetActive(false);
				Debug.DrawRay(pointer.position, pointer.forward * hit.distance, Color.yellow);
				Lines.CurrentStar = null;
			}
		}
	}
}
