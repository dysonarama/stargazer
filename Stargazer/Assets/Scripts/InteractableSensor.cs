﻿using Liminal.SDK.VR.Avatars;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSensor : MonoBehaviour
{
	private void Update()
	{
		//We want the actual laser pointer
		var pointer = VRAvatar.Active.PrimaryHand.GetControllerVisual().PointerVisual.Pointer.Transform;
		Debug.DrawRay(pointer.position, pointer.forward * 100);
	}
}
