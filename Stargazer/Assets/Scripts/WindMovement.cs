﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindMovement : MonoBehaviour
{
	public AudioSource WindAudio;
	public AudioClip InhaleAudio;
	public AudioClip ExhaleAudio;
	public float InhalationLength;
	public float ExhalationLength;
	public float Timer;
	public float RoundTimer;
	public float RestLength;
	public float StageLength;
	public int Stage;

	public float RoundInhalationReduction = 0.4f;
	public float RoundExhalationReduction = 0.9f;
	public float RoundRestRudiction = 0.1f;

	private bool InhaleResting;
	private bool ExhaleResting;
	private bool Exhaling;
	private bool Inhaling;

	public Image LeftBreatheIcon;
	public Image RightBreatheIcon;

	public float LeftIn;
	public float LeftOut;

	public float RightIn;
	public float RightOut;

	public Text MainText;
	public string EndingMessage;

	private void Start()
	{
		Timer = InhalationLength;
		RoundTimer = StageLength;
		Inhaling = true;
		StartCoroutine(Breath(LeftOut, LeftIn, RightOut, RightIn, InhalationLength));
		StartCoroutine(IconFade(0.5f, 10));
	}

	// Update is called once per frame
	void Update()
	{
		Timer -= Time.deltaTime;
		RoundTimer -= Time.deltaTime;

		if(Timer <= 0)
		{
			//sets the time for exhaling after and inhale rest
			if(InhaleResting)
			{
				print("Inhaling rest");
				Timer = ExhalationLength;
				InhaleResting = false;
				Exhaling = true;
				StartCoroutine(Breath(LeftIn,LeftOut,RightIn,RightOut,ExhalationLength));
			}

			//sets the time for inhaling after resting
			else if(ExhaleResting)
			{
				print("Exhaling rest");
				Timer = InhalationLength;
				ExhaleResting = false;
				Inhaling = true;
				StartCoroutine(Breath(LeftOut, LeftIn, RightOut, RightIn, InhalationLength));
				StartCoroutine(WindSoundFade(WindAudio, InhalationLength, 1));
			}

			//sets the rest time after exhaling
			else if(Exhaling)
			{
				print("Exhaling");
				Timer = RestLength;
				Exhaling = false;
				ExhaleResting = true;
				
			}

			//sets the rest time after inhaling
			else if(Inhaling)
			{
				print("Inhaling");
				Timer = RestLength;
				Inhaling = false;
				InhaleResting = true;
				StartCoroutine(WindSoundFade(WindAudio, ExhalationLength, 0.2f));
			}
		}

		//Sets the times for each phase of breathing once the new round begins. 
		if(RoundTimer <= 0)
		{
			Stage += 1;

			//fades the breathing icons to 50%
			if(Stage == 1)
			{
				StartCoroutine(IconFade(0.1f, 15));
			}


			if (Stage <= 3)
			{
				print("New Round");
				InhalationLength += RoundInhalationReduction;
				ExhalationLength += RoundExhalationReduction;
				RestLength += RoundRestRudiction;
			}
			RoundTimer = StageLength;

			if(Stage == 3)
			{
				StartCoroutine(IconFade(0, 5));
				GetComponent<BeginningSetup>().EndingMessage();
			}
		}
	}

	//Lerps the left and right breath icon in and out based on the breath timing.
	IEnumerator Breath(float LeftNewPosition, float LeftCurrentPosition, float RightNewPosition, float RightCurrentPosition, float duration)
	{
		
		print("Coroutine working");
		float ElapsedTime = 0;
		RectTransform LeftSide = LeftBreatheIcon.rectTransform;
		float LeftX = LeftSide.position.x;
		RectTransform RightSide = RightBreatheIcon.rectTransform;
		float RightX = RightSide.position.x;

		float Width = LeftBreatheIcon.rectTransform.rect.width;
		float Height = LeftBreatheIcon.rectTransform.rect.height;

		while (ElapsedTime < duration)
		{
			LeftX = Mathf.Lerp(LeftCurrentPosition, LeftNewPosition, (ElapsedTime / duration));
			RightX = Mathf.Lerp(RightCurrentPosition, RightNewPosition, (ElapsedTime / duration));
			RightSide.position = new Vector3(RightX,0,0);
			RightBreatheIcon.rectTransform.localPosition = RightSide.position;
			LeftSide.position = new Vector3(LeftX, 0, 0);
			LeftBreatheIcon.rectTransform.localPosition = LeftSide.position;
			Width = 30 * (RightX / 80);
			Height = 75 * (RightX / 80);
			RightBreatheIcon.rectTransform.sizeDelta = new Vector2(Width, Height);
			LeftBreatheIcon.rectTransform.sizeDelta = new Vector2(Width, Height);
			ElapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		
	}

	IEnumerator WindSoundFade(AudioSource audioSource, float FadeTime, float EndVolume)
	{
		float startVolume = audioSource.volume;
		float elapsedTime = 0;

		while(elapsedTime < FadeTime)
		{
			audioSource.volume = Mathf.Lerp(startVolume, EndVolume, (elapsedTime / FadeTime));
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
	}

	//Fades the breathing icons so as to not be so intrusive to the players vision.
	IEnumerator IconFade(float Alpha, float Duration)
	{
		float LeftAlpha = LeftBreatheIcon.GetComponent<Image>().color.a;

		float elapsedTime = 0;

		while(elapsedTime < Duration)
		{
			Color NewColor = new Color(1, 1, 1, Mathf.Lerp(LeftAlpha, Alpha, elapsedTime/Duration));
			LeftBreatheIcon.GetComponent<Image>().color = NewColor;
			RightBreatheIcon.GetComponent<Image>().color = NewColor;
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}
}
