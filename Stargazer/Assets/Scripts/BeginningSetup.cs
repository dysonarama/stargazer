﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeginningSetup : MonoBehaviour
{
	public StarLinesManager Lines;
	public Radio RadioScript;
	public WindMovement Breathing;
	public GameObject ReadyPanel;
	public GameObject ReadyObject;
	public Text MainText;
	public GameObject FinishButton;
	public GameObject ContinueButton;

	public float FadeDuration;

	public float IconExplanationLength;
	public float BreathingWithSizeLength;
	public float BreathInLength;
	public float BreathOutLength;
	public float Pause;
	public float RadioLocationLength;
	public float ChangeMusicTrackLength;
	public float AdjustVolumeLength;
	public float PointControllerLength;
	public float ConnectStarsLength;
	public float FinishedDrawingLength;

	public string BreathExplanation;
	public string BreathingWithSize;
	public string BreathIn;
	public string BreathOut;
	public string RadioLocationText;
	public string ChangingMusicTrack;
	public string AdjustingVolume;
	public string PointController;
	public string ConnectStars;
	public string FinishedDrawing;
	public string EndingMessageText;

	private float Breaths;

	public void StartExperience()
	{
		print("experience started");
		Breathing.enabled = true;
		ReadyObject.SetActive(false);
		StartCoroutine(Icon());
	}

	//One shot explanation of what the icons are
	IEnumerator Icon()
	{
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = BreathExplanation;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(IconExplanationLength);
		StartCoroutine(IconSize());
	}

	//One shot explanation of breathing with the icon size
	IEnumerator IconSize()
	{
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = BreathingWithSize;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(BreathingWithSizeLength);
		StartCoroutine(BreathInCo());
	}

	//Coroutine for Breath in sign to show up.
	IEnumerator BreathInCo()
	{
		StartCoroutine(Fade(0f, 1, MainText));
		yield return new WaitForSeconds(1);
		MainText.text = BreathIn;
		StartCoroutine(Fade(1f, 1, MainText));
		yield return new WaitForSeconds(BreathInLength);
		StartCoroutine(BreathOutCo());
	}

	//Courtine for Breath out sign to show up.
	IEnumerator BreathOutCo()
	{
		Breaths += 1;
		StartCoroutine(Fade(0f, 1, MainText));
		yield return new WaitForSeconds(1);
		MainText.text = BreathOut;
		StartCoroutine(Fade(1f, 1, MainText));
		yield return new WaitForSeconds(BreathOutLength);

		if (Breaths < 2)
		{
			StartCoroutine(BreathInCo());
		}

		else
		{		
			StartCoroutine(PauseCo(RadioLocation()));
		}
	}

	//A reusable coroutine for whenever a pause is needed in the explanation. Must send through a coroutine for it to go to.
	IEnumerator PauseCo(IEnumerator NextCoRoutine)
	{
		StartCoroutine(Fade(0, FadeDuration, MainText));
		yield return new WaitForSeconds(Pause);
		StartCoroutine(NextCoRoutine);
	}

	IEnumerator RadioLocation ()
	{
		RadioScript.enabled = true;
		MainText.text = RadioLocationText;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(RadioLocationLength);
		StartCoroutine(ChangingMusic());
	}

	//Oneshot coroutine for explaining how to change the music.
	IEnumerator ChangingMusic()
	{
		RadioScript.enabled = true;
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = ChangingMusicTrack;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(ChangeMusicTrackLength);
		StartCoroutine(Volume());
	}

	//Oneshot coroutine for explaining how to adjust the volume.
	IEnumerator Volume()
	{
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = AdjustingVolume;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(AdjustVolumeLength);
		StartCoroutine(PauseCo(DrawingStars()));
	}

	//Oneshot Coroutine for explaining that the player can click on stars
	IEnumerator DrawingStars()
	{
		Lines.enabled = true;
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = PointController;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(PointControllerLength);
		StartCoroutine(DragStars());
	}

	//Oneshot coroutine for explaining that the player can drag to other stars to draw
	IEnumerator DragStars()
	{
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = ConnectStars;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(ConnectStarsLength);
		StartCoroutine(PauseCo(DrawingComplete()));
	}

	//Oneshot coroutine for explaining how to finish the drawing off
	IEnumerator DrawingComplete()
	{
		StartCoroutine(Fade(0f, FadeDuration, MainText));
		yield return new WaitForSeconds(FadeDuration);
		MainText.text = FinishedDrawing;
		StartCoroutine(Fade(1f, FadeDuration, MainText));
		yield return new WaitForSeconds(FinishedDrawingLength);
		StartCoroutine(Fade(0f, FadeDuration, MainText));
	}

	//An adjustable fade coroutine
	IEnumerator Fade(float Alpha, float Duration, Text text)
	{
		float LeftAlpha = text.color.a;

		Color TextColor = text.color;

		float elapsedTime = 0;

		while (elapsedTime < Duration)
		{
			Color NewColor = new Color(text.color.r, text.color.g, text.color.b, Mathf.Lerp(LeftAlpha, Alpha, elapsedTime / Duration));
			text.color = NewColor;
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator FadeButtons(float Alpha, float Duration, GameObject Button)
	{
		float LeftAlpha = Button.GetComponent<MeshRenderer>().material.color.a;

		Color TextColor = Button.GetComponent<MeshRenderer>().material.color;

		float elapsedTime = 0;

		while (elapsedTime < Duration)
		{
			Color NewColor = new Color(Button.GetComponent<MeshRenderer>().material.color.r, Button.GetComponent<MeshRenderer>().material.color.g, Button.GetComponent<MeshRenderer>().material.color.b, Mathf.Lerp(LeftAlpha, Alpha, elapsedTime / Duration));
			Button.GetComponent<MeshRenderer>().material.color = NewColor;
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		Button.SetActive(!isActiveAndEnabled);
	}

	//Sets the end message of the game
	public void EndingMessage()
	{
		MainText.text = EndingMessageText;
		StartCoroutine(Fade(1f, 3f, MainText));
		FinishButton.SetActive(true);
		ContinueButton.SetActive(true);
		Color NormalColor = FinishButton.GetComponent<Material>().color;
		Color BeginColor = new Color(NormalColor.r, NormalColor.g, NormalColor.b, 0);
		FinishButton.GetComponent<Material>().color = BeginColor;
		FinishButton.GetComponentInChildren<Material>().color = BeginColor;
		ContinueButton.GetComponent<Material>().color = BeginColor;
		ContinueButton.GetComponentInChildren<Material>().color = BeginColor;
		StartCoroutine(FadeButtons(1, 1, FinishButton));
		StartCoroutine(FadeButtons(1, 1, FinishButton.transform.GetChild(0).gameObject));
		StartCoroutine(FadeButtons(1, 1, ContinueButton));
		StartCoroutine(FadeButtons(1, 1, ContinueButton.transform.GetChild(0).gameObject));
	}

	public void Continue()
	{
		StartCoroutine(Fade(0, 3, MainText));
		StartCoroutine(FadeButtons(0, 1, FinishButton));
		StartCoroutine(FadeButtons(0, 1, FinishButton.transform.GetChild(0).gameObject));
		StartCoroutine(FadeButtons(0, 1, ContinueButton));
		StartCoroutine(FadeButtons(0, 1, ContinueButton.transform.GetChild(0).gameObject));
	}
}
