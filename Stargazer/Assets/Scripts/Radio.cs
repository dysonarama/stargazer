﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{
	public AudioClip[] Songs;
	private AudioSource CurrentSong;
	public int SongNumber = 1;

	public bool OnUp;
	public bool OnDown;
	public bool OnLeft;
	public bool OnRight;


	// Use this for initialization
	void Start()
	{
		CurrentSong = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
		if (OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote).x > 0.5f)
		{
			if (Songs.Length == SongNumber)
			{
				SongNumber = 1;
			}

			else
			{
				SongNumber += 1;
			}

			print(SongNumber);

			StartCoroutine(SongChange());

			OnRight = false;
		}

		if (OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote).x < -0.5f)
		{

			if (SongNumber == 1)
			{
				SongNumber = Songs.Length;
			}

			else
			{
				SongNumber -= 1;
			}
			print(SongNumber);

			StartCoroutine(SongChange());
		}

		if(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote).y < -0.5f)
		{
			CurrentSong.volume -= 0.1f;
		}

		if (OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote).y > 0.5f)
		{

			print("Changing volume");
			if (CurrentSong.volume < 80)
			{
				CurrentSong.volume += 0.1f;
			}
		}

		// Pressing the right arrow on the control changes the track of the music
		if (OnRight)
		{
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.One))
			{
				if (Songs.Length == SongNumber)
				{
					SongNumber = 1;
				}

				else
				{
					SongNumber += 1;
				}

				print(SongNumber);

				StartCoroutine(SongChange());

				OnRight = false;
			}
		}
		
		//Pressing the left arrow on the control back the track of the music.
		if (OnLeft)
		{
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.One))
			{
				if (SongNumber == 1)
				{
					SongNumber = Songs.Length;
				}

				else
				{
					SongNumber -= 1;
				}
				print(SongNumber);

				StartCoroutine(SongChange());
			}
		}

		if (OnDown)
		{
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.One))
			{
				CurrentSong.volume -= 0.1f;
			}
		}

		if (OnUp)
		{
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.One))
			{

				print("Changing volume");
				if (CurrentSong.volume < 80)
				{
					CurrentSong.volume += 0.1f;
				}
			}
		}
	}

	//Coroutine for song change with each song being played at the previous songs current duration.
	IEnumerator SongChange()
	{
		float CurrentLength = CurrentSong.time;
		CurrentSong.Stop();
		print(CurrentSong.time);
		CurrentSong.clip = Songs[SongNumber -1];
		if (CurrentLength >= CurrentSong.clip.length)
		{
			CurrentSong.time = 0;
		}

		else
		{
			print(CurrentLength);
			CurrentSong.time = CurrentLength;
		}
		CurrentSong.Play();
		
		yield return null;
	}
}
