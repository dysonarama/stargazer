﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableObjectSample : MonoBehaviour, IPointerClickHandler/*, IPointerEnterHandler*/
{

	public StarLinesManager Lines;

	void Start()
	{
		Lines = GameObject.Find("GameManager").GetComponent<StarLinesManager>();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		//print(eventData.selectedObject.name);

		Lines.MousePos = eventData.position;

		//Asks if it's hitting a star and then changes the line managers current star to it. 
		if (eventData.selectedObject.tag == "Star")
		{
			eventData.selectedObject.GetComponent<Transform>().localScale *= 5;
			print("Hit Star");
			//TouchingStar = true;
			Lines.CurrentStar = eventData.selectedObject.gameObject;
			//Debug.DrawRay(pointer.position, pointer.forward * hit.distance, Color.green);
		}

		//If it isn't hitting a star it tells the line manager that there is no star there currently.
		else
		{
			//Debug.DrawRay(pointer.position, pointer.forward * hit.distance, Color.yellow);
			//TouchingStar = false;
			print("Hit Star Plane");
			Lines.CurrentStar = null;
		}
	}

	//public void OnPointerEnter(PointerEventData eventData)
	//{
	//	if (eventData.selectedObject.tag == "Star")
	//	{
	//		eventData.selectedObject.GetComponent<Transform>().localScale *= 10;
	//	}
	//}
}
