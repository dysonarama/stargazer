﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawning : MonoBehaviour
{
	public GameObject StarObject;
	public int AmountOfStars;
	public Transform SpawnObjectPos;
	private Vector3 SpawnPos;
	public float XDistance;
	public float ZDistance;
	public float YDistance;

	// Use this for initialization
	void Awake()
	{
		// starts the spawning at the centre of the scene
		SpawnPos = SpawnObjectPos.transform.position;

		// spawns the stars on a dome around the player.
		for (int i = 0; i < AmountOfStars; i++)
		{
			//randomises the position for the star to spawn
			Vector3 Spawn = new Vector3(Random.Range(-XDistance, XDistance), Random.Range(0, YDistance) , Random.Range(-ZDistance, ZDistance));

			//normalizes the position
			Vector3 NormalizedSpawn = Vector3.Normalize(Spawn);

			//changes the spawn vector 3 to be the multiplications of the sphere radius.
			Spawn = NormalizedSpawn * 20;

			//spawns the star in the spawn position.
			Instantiate(StarObject, Spawn, Quaternion.identity);
		}
	}
}
